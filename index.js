let express = require('express');
let app = express();

app
    .use(express.static(__dirname + '/dist'))
    .all('/*', function (req, res) {
        res.sendFile(__dirname + '/dist/index.html');
    })
    .listen(8080, 'localhost', function () {
        console.log(`Server started on port 8080`)
    })