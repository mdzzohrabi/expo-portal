let Locale = {
    lang: localStorage['lang'] || 'fa',
    fa: {
        'language_name': 'فارسی',
        Username: 'نام کاریری',
        Password: 'رمز عبور',
        Login: 'ورود به سیستم',
        invalidCredentials: 'نام کاربری یا رمز عبور اشتباه است',
        successLogin: 'با موفقیت وارد سیستم شدید',
        'Expos list': 'لیست نمایشگاه ها',
        addExpo: 'افزودن نمایشگاه',
        Loading: 'در حال بارگزاری ...',
        Edit: 'ویرایش',
        Stands: 'غرفه ها',
        Photos: 'عکس ها',
        Category: 'دسته بندی',
        Title: 'عنوان',
        Place: 'مکان',
        Expo: 'نمایشگاه',
        Name: 'نام',
        'Stand No': 'شماره غرفه',
        Address: 'آدرس',
        Company: 'شرکت',
        Manage: 'مدیریت',
        Description: 'توضیحات',
        'Add stand': 'افزودن غرفه',
        'Add company': 'افزودن شرکت',
        Logout: 'خروج از سیستم',
        Country: 'کشور',
        Province: 'استان',
        City: 'شهر',
        'Edit stand': 'ویرایش غرفه',
        Medias: 'محتواهای تصویری',
        User: 'کاربر',
        Stand: 'غرفه',
        Approved: 'تایید شده',
        Likes: 'لایک ها',
        Visits: 'نمایش ها',
        Size: 'اندازه',
        Media: 'رسانه',
        'Approved On': 'تاریخ تایید',
        'Created On': 'تاریخ ایجاد',
        'Are you sure to delete selected items ?': 'از حذف آیتم های انتخاب شده مطمئن هستید ؟',
        'Yes, Delete': 'بله، حذف',
        'Cancel': 'انصراف',
        Delete: 'حذف',
        'Approve/DisApprove': 'تایید/رد کردن',
        'Approve': 'تایید کردن',
        'Dis-Approve': 'رد کردن',
        Type: 'نوع',
        'Short Description': 'توضیحات کوتاه',
        'Long Description': 'توضیحات کامل',
        Logo: 'لوگو',
        Phones: 'تلفن تماس',
        Email: 'ایمیل',
        Website: 'وب سایت',
        'New Company': 'شرکت جدید',
        'Submit Company': 'ثبت شرکت',
        'Please enter company name': 'لطفا نام شرکت را وارد کنید',
        'Edit company': 'ویرایش شرکت',
        'Companies': 'شرکت ها',
        'Companies list': 'لیست شرکت ها',
        'companies': 'شرکت',
        'Stores': 'فروشگاه ها',
        'Add store': 'افزودن فروشگاه',
        'Edit store': 'ویرایش فروشگاه',
        'OnlineStore': 'فروشگاه آنلاین',
        'Store': 'فروشگاه',
        'Stores list': 'لیست فروشگاه ها',
        'Submit store': 'ثبت فروشگاه',
        'Expos': 'نمایشگاه ها',
        'Edit product': 'ویرایش محصول',
        'Status': 'وضعیت',
        'Active': 'فعال',
        'DeActive': 'غیرفعال',
        'Product': 'محصول',
        'Submit changes': 'اعمال تغییرات',
        'Products': 'محصولات',
        'Add product': 'افزودن محصول',
        'Products list': 'لیست محصولات',
        'Persian': 'فارسی',
        'Language': 'زبان',
        'dir': 'rtl',
        'Add': 'افزودن',
        'Price': 'قیمت',
        'Link': 'لینک',
        'Online': 'آنلاین',
        'Local': 'محلی'
    },
    en: {
        'dir': 'ltr',
        'language_name': 'English',
        'OnlineStore': 'Online store'
    }
}

export function trans(message: string) {
    return Locale[Locale.lang][message] || message;
}

export function getLocale() {
    return Locale.lang;
}

export function setLocale(lang) {
    Locale.lang = lang;
    localStorage['lang'] = lang;
}

export default new Proxy({}, {
    get(target, key) {
        return trans(String(key));
    }
})