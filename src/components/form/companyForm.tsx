import { Form, Input, Button } from "antd";
import { FormComponentProps } from "antd/lib/form";
import * as React from "react";
import { trans } from "../../strings";

export let CompanyForm = Form.create<{ company: any, onSubmit?: Function, buttonCaption?: string } & FormComponentProps>({
    name: 'company',
    mapPropsToFields: ({ company }) => {
        let fields = {};
        for (let key in company) {
            fields[key] = Form.createFormField({ value: company[key] });
        }
        return fields;
    }
})
(class BaseCompanyForm extends React.Component<{ buttonCaption?: string, onSubmit?: Function } & FormComponentProps> {

    handleSubmit(e) {
        e.preventDefault();
        this.props.form.validateFields((err, values) => {
            if (!err)
                this.props.onSubmit && this.props.onSubmit(values);
        });
    }

    render() {

        let { form: { getFieldDecorator }, buttonCaption, onSubmit, ...rest } = this.props;

        return <Form labelCol={{ span: 24, md: 6 }} wrapperCol={{ span: 24, md: 18 }} onSubmit={this.handleSubmit.bind(this)} { ...rest }>
            <Form.Item label={ trans('Name') } required className="rtl">{getFieldDecorator('name', {
                rules: [
                    { required: true, message: trans('Please enter company name') }
                ]
            })(<Input/>)}</Form.Item>
            <Form.Item label={ trans('Type') } required className="rtl">{getFieldDecorator('type')(<Input/>)}</Form.Item>
            <Form.Item label={ trans('Logo') } className="rtl">{getFieldDecorator('logo', {
                valuePropName: 'files',
                getValueFromEvent: event => {
                    return event && event.target.files;
                }
            })(<Input type={'file'}/>)}</Form.Item>
            <Form.Item label={ trans('Place') } className="rtl" help={""}>
                <Input.Group compact className="ltr">
                    {getFieldDecorator('city')(<Input placeholder={trans('City')} style={{ width: '33.3%' }}/>)}
                    {getFieldDecorator('province')(<Input placeholder={trans('Province')} style={{ width: '33.3%' }}/>)}
                    {getFieldDecorator('country')(<Input placeholder={trans('Country')} style={{ width: '33.3%' }}/>)}
                </Input.Group>
            </Form.Item>
            <Form.Item label={ trans('Address') } className="rtl">{getFieldDecorator('address')(<Input/>)}</Form.Item>
            <Form.Item label={ trans('Phones') } className="rtl">{getFieldDecorator('phones')(<Input/>)}</Form.Item>
            <Form.Item label={ trans('Email') } className="rtl">{getFieldDecorator('email')(<Input/>)}</Form.Item>
            <Form.Item label={ trans('Website') } className="rtl">{getFieldDecorator('website')(<Input/>)}</Form.Item>
            <Form.Item label={ trans('Short Description') } className="rtl">{getFieldDecorator('description')(<Input.TextArea/>)}</Form.Item>
            <Form.Item label={ trans('Long Description') } className="rtl">{getFieldDecorator('longDescription')(<Input.TextArea/>)}</Form.Item>


            <Form.Item>
                <Button type="primary" htmlType="submit">{ trans( buttonCaption || 'Submit Company' ) }</Button>
            </Form.Item>
        </Form>
    }
})