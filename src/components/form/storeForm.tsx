import { Form, Input, Button, Select } from "antd";
import { FormComponentProps } from "antd/lib/form";
import * as React from "react";
import { trans } from "../../strings";

export let StoreForm = Form.create<{ store: any, onSubmit?: Function, buttonCaption?: string } & FormComponentProps>({
    name: 'store',
    mapPropsToFields: ({ store }) => {
        let fields = {};
        for (let key in store) {
            fields[key] = Form.createFormField({ value: store[key] });
        }
        return fields;
    }
})
(class BaseStoreForm extends React.Component<{ buttonCaption?: string, onSubmit?: Function } & FormComponentProps> {

    handleSubmit(e) {
        e.preventDefault();
        this.props.form.validateFields((err, values) => {
            if (!err)
                this.props.onSubmit && this.props.onSubmit(values);
        });
    }

    render() {

        let { form: { getFieldDecorator }, buttonCaption, onSubmit, ...rest } = this.props;

        return <Form labelCol={{ span: 24, md: 6 }} wrapperCol={{ span: 24, md: 18 }} onSubmit={this.handleSubmit.bind(this)} { ...rest }>
            <Form.Item label={ trans('Name') } required className="rtl">{getFieldDecorator('name', {
                rules: [
                    { required: true, message: trans('Please enter store name') }
                ]
            })(<Input/>)}</Form.Item>
            <Form.Item label={ trans('Type') } required className="rtl">{getFieldDecorator('Type')(<Select>
                <Select.Option key="Store">{ trans('Store') }</Select.Option>
                <Select.Option key="OnlineStore">{ trans('OnlineStore') }</Select.Option>
            </Select>)}</Form.Item>
            <Form.Item label={ trans('Logo') } className="rtl">{getFieldDecorator('logo', {
                valuePropName: 'files',
                getValueFromEvent: event => {
                    return event && event.target.files;
                }
            })(<Input type={'file'}/>)}</Form.Item>
            <Form.Item label={ trans('Place') } className="rtl" help={""}>
                <Input.Group compact className="ltr">
                    {getFieldDecorator('city')(<Input placeholder={trans('City')} style={{ width: '33.3%' }}/>)}
                    {getFieldDecorator('province')(<Input placeholder={trans('Province')} style={{ width: '33.3%' }}/>)}
                    {getFieldDecorator('country')(<Input placeholder={trans('Country')} style={{ width: '33.3%' }}/>)}
                </Input.Group>
            </Form.Item>
            <Form.Item label={ trans('Address') } className="rtl">{getFieldDecorator('address')(<Input/>)}</Form.Item>
            <Form.Item label={ trans('Phones') } className="rtl">{getFieldDecorator('phones')(<Input/>)}</Form.Item>
            <Form.Item label={ trans('Email') } className="rtl">{getFieldDecorator('email')(<Input/>)}</Form.Item>
            <Form.Item label={ trans('Website') } className="rtl">{getFieldDecorator('website')(<Input/>)}</Form.Item>
            <Form.Item label={ trans('Description') } className="rtl">{getFieldDecorator('description')(<Input.TextArea/>)}</Form.Item>

            <Form.Item>
                <Button type="primary" htmlType="submit">{ trans( buttonCaption || 'Submit store' ) }</Button>
            </Form.Item>
        </Form>
    }
})