import { Form, Input, Button } from "antd";
import { FormComponentProps } from "antd/lib/form";
import * as React from "react";
import { trans } from "../../strings";

export let ExpoForm = Form.create<{ expo: any, buttonCaption?: string } & FormComponentProps>({
    name: 'expo',
    mapPropsToFields: ({ expo }) => {
        let fields = {};
        for (let key in expo) {
            fields[key] = Form.createFormField({ value: expo[key] });
        }
        return fields;
    }
})
(class BaseExpoForm extends React.Component<{ buttonCaption?: string } & FormComponentProps> {
    render() {

        let { form: { getFieldDecorator }, buttonCaption } = this.props;

        return <Form labelCol={{ span: 24, md: 6 }} wrapperCol={{ span: 24, md: 18 }}>
            <Form.Item label={ trans('Title') } className="rtl">{getFieldDecorator('title')(<Input/>)}</Form.Item>
            <Form.Item label={ trans('Category') } className="rtl">{getFieldDecorator('category')(<Input/>)}</Form.Item>
            <Form.Item label={ trans('Place') } className="rtl">
                <Input.Group compact className="ltr">
                    {getFieldDecorator('city')(<Input style={{ width: '33.3%' }}/>)}
                    {getFieldDecorator('province')(<Input style={{ width: '33.3%' }}/>)}
                    {getFieldDecorator('country')(<Input style={{ width: '33.3%' }}/>)}
                </Input.Group>
            </Form.Item>
            <Form.Item label={ trans('Address') } className="rtl">{getFieldDecorator('address')(<Input/>)}</Form.Item>
            <Form.Item label={ trans('Category') } className="rtl">{getFieldDecorator('category')(<Input/>)}</Form.Item>
            <Form.Item label={ trans('Description') } className="rtl">{getFieldDecorator('description')(<Input.TextArea/>)}</Form.Item>

            <Form.Item>
                <Button type="primary">{ trans( buttonCaption || 'Submit Expo' ) }</Button>
            </Form.Item>
        </Form>
    }
})