import { Form, Input, Button, Select, Row, Col, Modal } from "antd";
import { FormComponentProps } from "antd/lib/form";
import * as React from "react";
import { trans } from "../../strings";
import { companies, newCompany } from "../../api";
import { Company } from "../../typeDefs";
import { CompanyForm } from "./companyForm";

export let StandForm = Form.create<{ disabled?: boolean, stand: any, onSubmit?: Function, buttonCaption?: string } & FormComponentProps>({
    name: 'stand',
    mapPropsToFields: ({ stand }) => {
        let fields = {};
        for (let key in stand) {
            let value = stand[key];
            if (toString.call(value) == '[object Object]' && 'id' in value) {
                value = value.id;
            }
            fields[key] = Form.createFormField({ value });
        }
        return fields;
    }
})
(class BaseStandForm extends React.Component<{ buttonCaption?: string, onSubmit?: Function, disabled?: boolean } & FormComponentProps> {

    state = { companies: [] as Company[], companyModal: false }

    handleSubmit(e) {
        e.preventDefault();
        this.props.form.validateFields((err, values) => {
            if (!err)
                this.props.onSubmit && this.props.onSubmit(values);
        });
    }    

    componentDidMount() {
        return this.loadCompanies();
    }

    async loadCompanies(filter?: string) {
        this.setState({ companies: await companies(filter) });
    }

    render() {

        let { form: { getFieldDecorator }, buttonCaption, onSubmit, disabled, ...rest } = this.props;
        let { companies, companyModal } = this.state;

        return <React.Fragment>
                <Form labelCol={{ span: 24, md: 6 }} wrapperCol={{ span: 24, md: 18 }} onSubmit={this.handleSubmit.bind(this)} { ...rest }>
                <Form.Item label={ trans('Name') } className="rtl">{getFieldDecorator('name')(<Input disabled={disabled}/>)}</Form.Item>
                <Form.Item label={ trans('Company') } className="rtl">
                    <Row>
                        <Col span={ 18 }>
                        {getFieldDecorator('company')(<Select disabled={disabled}
                            showSearch
                            onSearch={(value) => {
                                this.loadCompanies(value);
                            }}
                            filterOption={(input, option) => {
                                return (option.props.children as string).toLowerCase().indexOf(input.toLowerCase()) >= 0;
                            }}
                        >
                            { companies.map(company => <Select.Option key={company.id} value={ company.id }>{ company.name }</Select.Option> ) }
                        </Select>)}
                        </Col>
                        <Col span={ 6 }>
                            &nbsp;<Button disabled={disabled} onClick={() => this.setState({ companyModal: true })}>{trans('Add company')}</Button>&nbsp;
                        </Col>
                    </Row>
                </Form.Item>
                <Form.Item label={ trans('Stand No') } className="rtl">{getFieldDecorator('standNo')(<Input disabled={disabled}/>)}</Form.Item>
                <Form.Item label={ trans('Address') } className="rtl">{getFieldDecorator('address')(<Input disabled={disabled}/>)}</Form.Item>
                <Form.Item label={ trans('Description') } className="rtl">{getFieldDecorator('description')(<Input.TextArea disabled={disabled}/>)}</Form.Item>
                <Form.Item>
                    <Button type="primary" htmlType="submit" disabled={disabled}>{ trans( buttonCaption || 'Submit Stand' ) }</Button>
                </Form.Item>
            </Form>
            <Modal visible={companyModal} footer={null} title={trans('New Company')} onCancel={() => this.setState({ companyModal: false })}>
                <CompanyForm
                    company={{  }}
                    onSubmit={(data: Company) => {
                        let { logo } = data;
                        delete data.logo;
                        data.showInCompanies = false;
                        newCompany(data, logo ? (logo as FileList).item(0) : undefined ).then(() => {
                            this.loadCompanies();
                            this.setState({ companyModal: false });
                        });
                    }}
                />
            </Modal>
        </React.Fragment>
    }
})