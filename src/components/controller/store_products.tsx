import { RouteComponentProps } from "react-router-dom";
import { PageController } from "./pageController";
import { trans } from "../../strings";
import { useState, useEffect, useMemo } from "react";
import { Store, Product, ProductPrice } from "../../typeDefs";
import { StoreApi, ProductPriceApi } from "../../api";
import { Table, Descriptions, Select, Input, Button } from "antd";
import { ColumnProps } from "antd/lib/table";
import * as React from 'react';
import { useInputChange } from "../../util";

export function StoreProductsController(props: RouteComponentProps<any>) {

    let [loading, setLoading] = useState(true);
    let [store, setStore] = useState<Store>({});
    let [products, setProducts] = useState<Product[]>([]);
    let [prices, setPrices] = useState<ProductPrice[]>([]);
    let [price, setPrice, changePriceInput] = useInputChange<Partial<ProductPrice>>({ buyType: 'Online' });
    let columns = useMemo(() => ([
        {
            title: trans('Product'),
            dataIndex: 'product.name',
            key: 'product'
        },
        {
            title: trans('Price'),
            dataIndex: 'price',
            key: 'price'
        },
        {
            title: trans('Type'),
            dataIndex: 'buyType',
            key: 'type'
        },
        {
            title: trans('Link'),
            dataIndex: 'link',
            key: 'link'
        },
        // {
        //     title: trans('Products'),
        //     key: 'products',
        //     render: (value: string, row: Store) => 
        // }
    ] as ColumnProps<Product>[]), []);

    let loadData = () => {
        setLoading(true);
        Promise.all([
            ProductPriceApi.list(props.match.params.id),
            StoreApi.get(props.match.params.id)
        ]).then(([prices, store]) => {
            setProducts(store.products);
            setPrices(prices);
            setStore(store);
            setLoading(false);
        })
    }

    useEffect(() => {
        loadData();        
    }, [props.match.params.id]);

    let addPrice = React.useCallback(() => {
        price.store = store;
        ProductPriceApi.add(price as any).then(loadData);
    }, [price]);

    return <PageController {...props} title={store.name} subTitle={trans("Products")} loading={loading}>
        <Table
            className="ltr"
            dataSource={prices}
            columns={columns}
            rowKey={price => price.id}
        />
        <Descriptions bordered>
            <Descriptions.Item label={trans("Product")}>
                <Select style={{ width: 300 }} value={price.product?.id} onChange={value => setPrice({ ...price, product: products.find(p => p.id == value) })}>
                    {products.map(product => {
                        return <Select.Option key={product.id}>{product.name}</Select.Option>
                    })}
                </Select>
            </Descriptions.Item>
            <Descriptions.Item label={trans('Price')}>
                <Input name="price" onChange={changePriceInput} value={price.price}/>
            </Descriptions.Item>
            <Descriptions.Item label={trans('Link')}>
                <Input name="link" dir="ltr" placeholder="http://" onChange={changePriceInput} value={price.link}/>
            </Descriptions.Item>
            <Descriptions.Item label={trans('Type')}>
                <Select value={price.buyType} onChange={value => setPrice({ ...price, buyType: value })} style={{ width: 200 }}>
                    <Select.Option key={"Online"}>{trans('Online')}</Select.Option>
                    <Select.Option key={"Local"}>{trans('Local')}</Select.Option>
                </Select>
            </Descriptions.Item>
            <Descriptions.Item label={trans('-')}>
                <Button onClick={addPrice} type="primary" icon="plus">{trans("Add")}</Button>
            </Descriptions.Item>
        </Descriptions>
    </PageController>

}