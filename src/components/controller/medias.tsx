import * as React from "react";
import { MediaManager } from "../mediaManager";
import { PageController } from "./pageController";
import { trans } from "../../strings";
import { RouteComponentProps } from "react-router";

export class MediasController extends React.Component<RouteComponentProps> {

    render() {
        return <PageController
            title={ trans('Medias') }
            { ...this.props }
        >
            <MediaManager
                upload={false}
            />
        </PageController>
        
    }

}