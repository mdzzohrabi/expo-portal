import * as React from 'react';
import { Component } from "react";
import { RouteComponentProps } from 'react-router-dom';
import { trans } from '../../strings';
import { MediaManager } from '../mediaManager';
import { PageController } from './pageController';
import { stand } from '../../api';
import { message } from 'antd';

export class StandController extends Component<RouteComponentProps<{ standId: string }>> {

    state = {
        stand: {} as any,
        loading: true
    }

    get standId() {
        return this.props.match.params.standId;
    }

    componentDidMount() {
        this.loadStand();
    }

    loadStand() {
        stand(this.standId)
            .then(stand => this.setState({ stand, loading: false }))
            .catch(err => message.error(trans('Loading error')))
    }

    render() {
        let { stand } = this.state;
        return <PageController
            title={ trans("Stand") + ' - ' + stand.name }
            { ...this.props }
        >
            {/* <FileManager medias={ stand.images } upload={true} uploadRef="stand" uploadRefId={ this.standId } onUpload={this.loadStand.bind(this)}/> */}
            <MediaManager modelType={"stand"} modelId={ this.standId } upload={true}/>
        </PageController>
    }

}