import { Button, Modal, Table } from 'antd';
import { PaginationConfig } from 'antd/lib/table';
import { parse as parseQuery } from 'qs';
import * as React from 'react';
import { Component } from "react";
import { Link, RouteComponentProps } from 'react-router-dom';
import { assetUrl, getStoresGrid, newStore, editStore } from '../../api';
import strings, { trans } from '../../strings';
import { Store } from '../../typeDefs';
import { Sorter } from '../../util';
import { tableFilter } from '../tableFilter';
import { PageController } from './pageController';
import { StoreForm } from '../form/storeForm';
import { LoginController } from './login';

export class StoresController extends Component<RouteComponentProps> {

    mounted = false;

    state = {
        stores: [] as Store[] | undefined,
        columns: [
            {
                title: trans('Logo'),
                dataIndex: 'logo.url',
                key: 'logo',
                render: (value) => <img src={ assetUrl(value, 90)} style={{ width: '40px' }}/>
            },
            {
                title: trans('Name'),
                dataIndex: 'name',
                key: 'name',
                ...tableFilter(this, 'name'),
                sorter: Sorter.String('name')
            },
            {
                title: trans('Webstie'),
                dataIndex: 'website',
                key: 'website',
                ...tableFilter(this, 'website')
            },
            {
                title: trans('Country'),
                dataIndex: 'country',
                key: 'country',
                ...tableFilter(this, 'country')
            },
            {
                title: trans('Province'),
                dataIndex: 'province',
                key: 'province',
                ...tableFilter(this, 'province')
            },
            {
                title: trans('City'),
                dataIndex: 'city',
                key: 'city',
                ...tableFilter(this, 'city')
            },
            {
                title: trans('Type'),
                dataIndex: 'Type',
                key: 'Type',
                ...tableFilter(this, 'Type'),
                render: value => trans(value)
            },
            {
                title: trans('Products'),
                key: 'products',
                render: (value: string, row: Store) => <Link to={`/products/${ row.id }`}>{ trans('Products') }</Link>
            },
            {
                title: trans('Edit'),
                key: 'edit',
                render: (value: string, row: Store) => <Link to={`?edit=${ row.id }&page=${this.state.pagination.current}&size=${this.state.pagination.pageSize}`}>{ trans('Edit') }</Link>
            }
        ],
        store: {} as Store,
        loading: true,
        pagination: {
            current: 1,
            pageSize: 20,
            simple: false,
            pageSizeOptions: ['20', '50', '100'],
            showSizeChanger: true,
            showTotal: (total, range) => <span>{ total } { strings['stores'] }</span>
        } as PaginationConfig
    }

    componentDidMount() {
        this.mounted = true;
        this.loadStores();
    }

    loadStores() {

        let { current, pageSize } = this.state.pagination;

        let start = (current - 1) * pageSize;
        let limit = pageSize;

        this.setState({ loading: true });
        return getStoresGrid({}, start, limit).then(result => {
            if (!this.mounted) return;
            this.setState({
                loading: false,
                stores: result.values,
                pagination: {
                    ...this.state.pagination,
                    total: result.aggregate.totalCount
                }
            })
        });
    }

    componentWillUnmount() {
        this.mounted = false;
    }

    changePage = (pager: PaginationConfig, filters: any) => {
        this.props.history.push(`/stores?page=${pager.current}&size=${pager.pageSize}`);
    }

    rowKey = (row: any) => row.id;

    componentDidUpdate(prevProps: RouteComponentProps) {
        let { stores, pagination } = this.state;
        let { page, size } = parseQuery(this.props.location.search.substr(1));
        page = Number(page) || pagination.current;
        size = Number(size) || pagination.pageSize;

        if ( page != pagination.current || size != pagination.pageSize ) {
            this.setState({
                pagination: {
                    ...pagination,
                    current: page,
                    pageSize: size
                }
            }, () => this.loadStores());
        }
    }

    render() {

        let { stores, columns, loading, pagination } = this.state;
        let { history } = this.props;
        let store = undefined as Store | undefined;

        let { edit: editId, add } = parseQuery(this.props.location.search.substr(1));

        add = add !== undefined;

        if (editId) {
            store = stores.find(row => row.id == editId);
        }


        return <PageController
            title={ strings['Stores'] }
            subTitle={ strings['Stores list'] }
            extra={[
                LoginController.hasAcl('portal.addStore') ? <Button key="btn-new-store" onClick={ () => history.push('/stores?add') }>{ strings['Add store'] }</Button> : null
            ]}
            { ...this.props }
        >

            <Table
                loading={loading}
                className="ltr"
                dataSource={stores}
                columns={columns}
                rowKey={this.rowKey}
                pagination={pagination}
                onChange={this.changePage}
                />

            <Modal visible={!!store || add} onCancel={() => history.goBack()} footer={null}>
                <StoreForm
                    store={add ? {} : store}
                    buttonCaption={add ? strings['Submit store'] : strings['Edit store']}
                    onSubmit={(data: Store) => {
                        let { logo } = data;
                        delete data.logo;
                        let logoFile = logo instanceof FileList ? (logo as FileList).item(0) : undefined;

                        if ( add )
                            newStore(data, logoFile).then(() => {
                                this.loadStores();
                                history.push('/stores');
                            });
                        else
                            editStore({ id: editId, ...data }, logoFile).then(() => {
                                this.loadStores();
                                history.push('/stores');
                            });
                    }}
                />
            </Modal>
        </PageController>;
    }

}