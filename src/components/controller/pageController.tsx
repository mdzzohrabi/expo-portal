import { PageHeader, Row, Skeleton } from 'antd';
import { PageHeaderProps } from 'antd/lib/page-header';
import * as React from 'react';
import { Component } from "react";
import { RouteComponentProps } from 'react-router-dom';
import { PanelMenu } from '../panelMenu';

export interface PageControllerProps extends RouteComponentProps, PageHeaderProps {
    loading?: boolean
}

export class PageController extends Component<PageControllerProps> {

    render() {
        let { history, children, location, match, loading, ...attrs } = this.props;

        return <div className="page">
                <PanelMenu key="panel-menu"/>
                <PageHeader
                    onBack={() => history && history.goBack() }
                    { ...attrs }
                />
                <Row>
                    <Skeleton loading={loading} active>
                        { children }
                    </Skeleton>
                </Row>
        </div>
    }

}