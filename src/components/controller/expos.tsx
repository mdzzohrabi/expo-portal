import { Button, Spin, Table } from 'antd';
import * as React from 'react';
import { Component } from "react";
import { Link, RouteComponentProps } from 'react-router-dom';
import { expos } from '../../api';
import strings, { trans } from '../../strings';
import { ShowWhen } from '../showWhen';
import { PageController } from './pageController';
import { tableFilter } from '../tableFilter';

export class ExposPage extends Component<RouteComponentProps> {

    mounted = false;

    state = {
        expos: [],
        columns: [
            {
                title: trans('Title'),
                dataIndex: 'title',
                key: 'title',
                render: (title, expo) => {
                    return <Link to={"/expo/" + expo.id}>{ title }</Link>
                },
                ...tableFilter(this, 'title'),
                sorter: (a, b) => a.title > b.title ? 1 : -1
            },
            {
                title: trans('Country'),
                dataIndex: 'country',
                key: 'country',
                ...tableFilter(this, 'country')
            },
            {
                title: trans('Province'),
                dataIndex: 'province',
                key: 'province',
                ...tableFilter(this, 'province')
            },
            {
                title: trans('City'),
                dataIndex: 'city',
                key: 'city',
                ...tableFilter(this, 'city')
            },
            {
                title: trans('Category'),
                dataIndex: 'category',
                key: 'category',
                ...tableFilter(this, 'category')
            }
        ],
        loading: true
    }

    componentDidMount() {
        this.mounted = true;
        expos().then(expos => {
            this.mounted && this.setState({ expos, loading: false });
        })
    }

    componentWillUnmount() {
        this.mounted = false;
    }

    render() {

        let { expos, columns, loading } = this.state;
        let { history } = this.props;

        return <PageController
            title={ trans('Expos') }
            subTitle={ trans('Expos list') }
            // extra={[ <Button key="btn-new-expo" onClick={ () => history.push('/expos/add') }>{ strings.addExpo }</Button> ]}
            { ...this.props }
        >
            <Table
                loading={loading}
                className="ltr"
                dataSource={expos}
                columns={columns}
                rowKey={row => row.id}
                />
        </PageController>;
    }

}