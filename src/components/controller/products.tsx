import { Button } from 'antd';
import * as React from 'react';
import { Component } from "react";
import { Link, RouteComponentProps } from 'react-router-dom';
import { assetUrl, ProductApi } from '../../api';
import { trans } from '../../strings';
import { Product } from '../../typeDefs';
import { Sorter } from '../../util';
import { LazyTable } from '../lazy-table';
import { tableFilter } from '../tableFilter';
import { PageController } from './pageController';
import { LoginController } from './login';

export class ProductsController extends Component<RouteComponentProps> {

    state = {
        columns: [
            {
                title: trans('Poster'),
                dataIndex: 'poster.url',
                key: 'poster',
                render: (value) => <img src={ assetUrl(value, 90)} style={{ width: '40px' }}/>
            },
            {
                title: trans('Name'),
                dataIndex: 'name',
                key: 'name',
                ...tableFilter(this, 'name'),
                sorter: Sorter.String('name')
            },
            {
                title: trans('Description'),
                dataIndex: 'description',
                key: 'description',
                ...tableFilter(this, 'description')
            },
            {
                title: trans('Category'),
                dataIndex: 'category',
                key: 'category',
                ...tableFilter(this, 'category')
            },
            {
                title: trans('Company'),
                dataIndex: 'company.name',
                key: 'company',
                ...tableFilter(this, 'company.name')
            },
            {
                title: trans('Edit'),
                key: 'edit',
                render: (value: string, row: Product) => <Link to={`/product/${ row.id }`}>{ trans('Edit') }</Link>
            }
        ],
        product: {} as Product
    }

    render() {

        return <PageController
            title={ trans('Products') }
            subTitle={ trans('Products list') }
            extra={[
                LoginController.hasAcl('portal.addProduct') ? <Button key="btn-new-product" onClick={ () => this.props.history.push('/product/add') } icon="plus">{ trans('Add product') }</Button> : null
            ]}
            { ...this.props }
        >

            <LazyTable
                columns={this.state.columns}
                basePath="/products"
                dataSource={ProductApi.list}
                totalSuffix={trans('Products')}
                {...this.props}
            />
        </PageController>;
    }

}