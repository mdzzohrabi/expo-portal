import * as React from 'react';
import { Component } from "react";
import { Layout, Input, Row, Col, PageHeader, Button, message } from 'antd';
import strings, { trans } from '../../strings';
import { InputRow } from '../inputRow';
import { bindTo } from '../../util';
import * as api from '../../api';
import { RouteComponentProps } from 'react-router-dom';
import { User } from '../../typeDefs';

export class LoginController extends Component<RouteComponentProps> {

    state = { username: '', password: '', loading: false };

    static get isAuthenticated () {
        return (LoginController.StoredToken || '').length > 0 && LoginController.User != null;
    }

    static get StoredToken() {
        return localStorage.getItem('jwt');
    }

    static get User(): User {
        return JSON.parse( localStorage.getItem('user') );
    }

    static get isAdmin() {
        return LoginController.User.role.type == 'root';
    }

    static get StoredAcl() {
        return JSON.parse( localStorage.getItem('acl') ) || [];
    }

    static set StoredAcl(acl: string[]) {
        localStorage.setItem('acl', JSON.stringify(acl));
    }

    static hasAcl(name: string) {
        return LoginController.StoredAcl.includes(name);
    }

    static async LoadAcl(userId: string) {
        LoginController.StoredAcl = await api.getAcl( userId );
    }

    static logout() {
        localStorage.removeItem('jwt');
        localStorage.removeItem('user');
    }

    componentWillMount() {

        if (this.props.location.pathname.endsWith('logout')) {
            LoginController.logout();
            return this.props.history.push('/login');
        }

        if ( LoginController.isAuthenticated ) {
            api.createClient( LoginController.StoredToken );
            this.props.history.push('/');
        }
    }

    performLogin = async () => {
        let { username, password } = this.state;
        this.setState({ loading: true });

        try {
            let result = await api.login( username, password );
            localStorage.setItem('jwt', result.jwt);
            localStorage.setItem('user', JSON.stringify(result.user));
            api.createClient(result.jwt);
            await LoginController.LoadAcl(result.user.id);
            message.success(trans('successLogin'));
        } catch (err) {
            message.error(trans('invalidCredentials'))
        }

        this.setState({ loading: false });
        this.props.history.push('/');
    }

    render() {
        let { password, username, loading } = this.state;
        return <div className="login-container">
            <PageHeader title={ trans('Login') }/>

            <InputRow label={ trans('Username') }>
                <Input className="ltr" type="text" value={username} disabled={loading} onChange={bindTo(this, 'username')}/>
            </InputRow>

            <InputRow label={ trans('Password') }>
                <Input className="ltr" type="password" value={password} disabled={loading} onChange={bindTo(this, 'password')}/>
            </InputRow>

            <Row>
                <Button className="ltr" type="primary" loading={ loading } shape="round" icon="user" onClick={this.performLogin}>{ trans('Login') }</Button>
            </Row>
        </div>;
    }
}