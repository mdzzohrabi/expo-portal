import { message, Table } from "antd";
import * as React from "react";
import { RouteComponentProps, Redirect } from "react-router";
import { Link } from "react-router-dom";
import { addStand, stands, editStand } from "../../api";
import { trans } from "../../strings";
import { StandForm } from "../form/standForm";
import { tableFilter } from "../tableFilter";
import { Stand } from "../../typeDefs";
import { parse as parseQuery } from 'qs';
import { handleError } from "../../util";
import { ScrollIntoView } from '../scrollIntoView';
import { PaginationConfig } from "antd/lib/table";

export class StandsController extends React.Component<{ expoId: string } & RouteComponentProps> {

    state = {
        loading: true,
        stands: [] as Stand[],
        columns: [
            { title: trans('Name'), dataIndex: 'name', key: 'name', ...tableFilter(this, 'name') },
            { title: trans('Stand No'), dataIndex: 'standNo', key: 'standNo' },
            { title: trans('Address'), dataIndex: 'address', key: 'address' },
            { title: trans('Company'), dataIndex: 'company.name', key: 'company.name', ...tableFilter(this, 'company.name') },
            {
                title: trans('Manage'),
                key: 'manage',
                render: (value: string, row: Stand) => <Link to={`/stand/${ row.id }`}>{ trans('Manage') }</Link>
            },
            {
                title: trans('Edit'),
                key: 'edit',
                render: (value: string, row: Stand) => <Link to={`/expo/${ this.expoId }/stands?edit=${ row.id }#edit`}>{ trans('Edit') }</Link>
            }
        ],
        newStand: {} as Stand,
        standFormLoad: false,
        pagination: { simple: false } as PaginationConfig
    }

    get expoId() {
        return this.props.expoId;
    }


    componentDidMount() {
        return Promise.all([ this.loadStands()]);
    }

    loadStands(start?: number, limit?: number, where?: any) {
        this.setState({ loading: true });
        return stands(this.expoId, start, limit, where).then(stands => {
            this.setState({ stands: stands.values, loading: false, pagination: {
                ...this.state.pagination,
                total: stands.aggregate.totalCount
            } });
        })
    }

    addStand = async (newStand: Stand) => {

        let { history } = this.props;
        this.setState({ standFormLoad: true });
        let loading = message.loading(trans('Loading ...'));
        
        let { edit: editId } = parseQuery(this.props.location.search.substr(1));

        try {

            if ( editId ) {
                await editStand(editId as string, newStand);
                this.loadStands();
                history.push(`/expo/${ this.expoId }/stands`);
            } else {
                await addStand(this.expoId, newStand);
                message.success(trans( editId ? `Stand edited successfull` : `New stand added to expo`));
                this.loadStands();
            }

        } catch (e) { handleError(e) }

        loading();
        this.setState({ standFormLoad: false });
        
    }

    render() {
        let { loading, stands, columns, standFormLoad, pagination } = this.state;

        let { edit: editId } = parseQuery(this.props.location.search.substr(1));
        let { hash: scrollTo } = this.props.location;
        let stand: Stand = this.state.newStand;

        if (editId) {
            stand = stands.find(stand => stand.id == editId);
        }

        return <ScrollIntoView id={ scrollTo }>
            <Table
                className="ltr"
                loading={loading}
                dataSource={ stands }
                columns={ columns }
                rowKey={r => r.id}
                pagination={pagination}
                onChange={(pager, filters) => {

                    this.setState({
                        pagination: {
                            ...this.state.pagination,
                            current: pager.current
                        }
                    })

                    let where = {} as any;
                    if (filters.name) {
                        where.name_contains = filters.name.pop();
                    }

                    this.loadStands((pager.current - 1) * pager.pageSize, pager.pageSize, where);

                }}
            />

            <div id="edit" className="margin-horizontal">
                <h1>{ trans('Add stand') }</h1>

                <StandForm
                    buttonCaption={ trans(editId ? 'Edit stand' : 'Add stand') }
                    stand={stand}
                    onSubmit={this.addStand}
                    disabled={ standFormLoad }
                />
            </div>
        </ScrollIntoView>
    }
}