import { Spin, Menu, Icon, Col, Row, Statistic } from "antd";
import * as React from "react";
import { RouteComponentProps, Route, Link, Router, BrowserRouter, Switch } from "react-router-dom";
import { expo } from "../../api";
import strings, { trans } from "../../strings";
import { ExpoForm } from "../form/expoForm";
import { ShowWhen } from "../showWhen";
import { PageController } from "./pageController";
import { StandsController } from "./stands";
import { FileManager } from "../fileManager";
import { MediaManager } from "../mediaManager";

export class ExpoController extends React.Component<RouteComponentProps> {

    state = {
        loading: true,
        expo: {},
        edit: false
    } as {
        loading: boolean,
        expo: any,
        edit: boolean
    }

    get expoId() {
        return (this.props.match.params as any).id;
    }

    async componentDidMount() {
        await this.loadData();
    }

    async loadData() {
        if (this.expoId) {
            this.setState({ edit: true, loading: true });
            this.setState({ expo: await expo(this.expoId), loading: false });
        } else {
            this.setState({ loading: false });
        }
    }

    render() {
        let { loading, expo, edit } = this.state;
        let { match, location } = this.props;

        
        return <PageController
            title={ trans( loading ? strings.loading : edit ? expo.title : 'New Expo' ) }
            subTitle={ trans( edit ? 'Edit' : 'New' ) }
            footer={
                <Menu key="expo-menu" mode="horizontal" selectedKeys={[
                    match.isExact ? 'expo-info' : null,
                    location.pathname.endsWith('/edit') ? 'expo-edit' : null,
                    location.pathname.endsWith('/stands') ? 'expo-stands' : null,
                    location.pathname.endsWith('/photos') ? 'expo-photos' : null
                ]}>
                    <Menu.Item key="expo-info"><Link to={`/expo/${ this.expoId }/`}><Icon type="bars"/>{ trans('Expo') }</Link></Menu.Item>
                    {/* <Menu.Item key="expo-edit"><Link to={`/expo/${ this.expoId }/edit`}><Icon type="edit"/>{ strings.edit }</Link></Menu.Item> */}
                    <Menu.Item key="expo-stands"><Link to={`/expo/${ this.expoId }/stands`}><Icon type="build"/>{ strings.stands }</Link></Menu.Item>
                    <Menu.Item key="expo-photos"><Link to={`/expo/${ this.expoId }/photos`}><Icon type="instagram"/>{ strings.photos }</Link></Menu.Item>
                </Menu>
            }
            { ...this.props }
        >
            <ShowWhen expr={ !loading } otherwise={ <div className="text-center"><Spin size="default"/></div> }>
                <Switch>
                    <Route path={match.path + "/edit"} match render={() => <div className="margin-horizontal"><ExpoForm expo={ expo }/></div> }/>
                    <Route path={match.path + "/stands"} match render={() => <StandsController { ...this.props } expoId={ this.expoId }/> }/>
                    <Route path={match.path + "/photos"} match render={() => <MediaManager modelType={"expo"} modelId={ this.expoId } upload={true}/> }/>
                    {/* <Route path={match.path + "/photos"} match render={() => <FileManager medias={ expo.images } uploadRef={"Expo"} uploadRefId={ this.expoId } upload={true}/> }/> */}
                    <Route path={match.path + "/"} match render={() => {
                        return <React.Fragment>
                            <Row className="margin-horizontal" style={{ marginTop: 20 }}>
                            <Col span={24} md={12}>
                                <Statistic title={ trans('Title') } value={ expo.title }/>
                            </Col>
                            <Col span={24} md={12}>
                                <Statistic title={ trans('Category') } value={ expo.category }/>
                            </Col>
                            <Col span={24} md={12}>
                                <Statistic title={ trans('Place') } value=
                                { expo.country }/>
                            </Col>
                            </Row>
                        </React.Fragment>
                    }}/>
                </Switch>
            </ShowWhen>
        </PageController>
    }

}