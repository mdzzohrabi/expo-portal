import { Button, Spin, Table, Modal } from 'antd';
import * as React from 'react';
import { Component } from "react";
import { Link, RouteComponentProps } from 'react-router-dom';
import { expos, fetchCompaniesGrid, newCompany, editCompany, assetUrl } from '../../api';
import strings, { trans } from '../../strings';
import { ShowWhen } from '../showWhen';
import { PageController } from './pageController';
import { tableFilter } from '../tableFilter';
import { Sorter } from '../../util';
import { PaginationConfig } from 'antd/lib/table';
import { Company } from '../../typeDefs';
import { CompanyForm } from '../form/companyForm';
import { parse as parseQuery } from 'qs';
import { LoginController } from './login';

export class CompaniesController extends Component<RouteComponentProps> {

    mounted = false;

    state = {
        companies: [] as Company[] | undefined,
        columns: [
            {
                title: trans('Name'),
                dataIndex: 'name',
                key: 'name',
                ...tableFilter(this, 'name'),
                sorter: Sorter.String('name')
            },
            {
                title: trans('Logo'),
                dataIndex: 'logo.url',
                key: 'logo',
                render: (value) => <img src={ assetUrl(value, 90)} style={{ width: '40px' }}/>
            },
            {
                title: trans('Description'),
                dataIndex: 'description',
                key: 'description',
                ...tableFilter(this, 'description')
            },
            {
                title: trans('Country'),
                dataIndex: 'country',
                key: 'country',
                ...tableFilter(this, 'country')
            },
            {
                title: trans('Province'),
                dataIndex: 'province',
                key: 'province',
                ...tableFilter(this, 'province')
            },
            {
                title: trans('City'),
                dataIndex: 'city',
                key: 'city',
                ...tableFilter(this, 'city')
            },
            {
                title: trans('Type'),
                dataIndex: 'type',
                key: 'type',
                ...tableFilter(this, 'type')
            },
            {
                title: trans('Edit'),
                key: 'edit',
                render: (value: string, row: Company) => <Link to={`?edit=${ row.id }&page=${this.state.pagination.current}&size=${this.state.pagination.pageSize}`}>{ trans('Edit') }</Link>
            }
        ],
        company: {} as Company,
        loading: true,
        pagination: {
            current: 1,
            pageSize: 20,
            simple: false,
            pageSizeOptions: ['20', '50', '100'],
            showSizeChanger: true,
            showTotal: (total, range) => <span>{ total } { strings['companies'] }</span>
        } as PaginationConfig
    }

    componentDidMount() {
        this.mounted = true;
        this.loadCompanies();
    }

    loadCompanies() {

        let { current, pageSize } = this.state.pagination;

        let start = (current - 1) * pageSize;
        let limit = pageSize;

        this.setState({ loading: true });
        return fetchCompaniesGrid(start, limit).then(result => {
            if (!this.mounted) return;
            this.setState({
                loading: false,
                companies: result.values,
                pagination: {
                    ...this.state.pagination,
                    total: result.aggregate.totalCount
                }
            })
        });
    }

    componentWillUnmount() {
        this.mounted = false;
    }

    changePage = (pager: PaginationConfig, filters: any) => {
        this.props.history.push(`/companies?page=${pager.current}&size=${pager.pageSize}`);
    }

    rowKey = (row: any) => row.id;

    componentDidUpdate(prevProps: RouteComponentProps) {
        let { companies, pagination } = this.state;
        let { page, size } = parseQuery(this.props.location.search.substr(1));
        page = Number(page) || pagination.current;
        size = Number(size) || pagination.pageSize;

        if ( page != pagination.current || size != pagination.pageSize ) {
            this.setState({
                pagination: {
                    ...pagination,
                    current: page,
                    pageSize: size
                }
            }, () => this.loadCompanies());
        }
    }

    render() {

        let { companies, columns, loading, pagination } = this.state;
        let { history } = this.props;
        let company = undefined as Company | undefined;

        let { edit: editId, add } = parseQuery(this.props.location.search.substr(1));

        add = add !== undefined;

        if (editId) {
            company = companies.find(row => row.id == editId);
        }


        return <PageController
            title={ strings['Companies'] }
            subTitle={ strings['Companies list'] }
            extra={[
                LoginController.hasAcl('portal.addCompany') ? <Button key="btn-new-company" onClick={ () => history.push('/companies?add') }>{ strings['Add company'] }</Button> : null
            ]}
            { ...this.props }
        >

            <Table
                loading={loading}
                className="ltr"
                dataSource={companies}
                columns={columns}
                rowKey={this.rowKey}
                pagination={pagination}
                onChange={this.changePage}
                />

            <Modal visible={!!company || add} onCancel={() => history.goBack()} footer={null}>
                <CompanyForm
                    company={add ? {} : company}
                    buttonCaption={add ? strings['Submit Company'] : strings['Edit company']}
                    onSubmit={(data: Company) => {
                        let { logo } = data;
                        delete data.logo;
                        let logoFile = logo instanceof FileList ? (logo as FileList).item(0) : undefined;

                        if ( add )
                            newCompany(data, logoFile).then(() => {
                                this.loadCompanies();
                                history.push('/companies');
                            });
                        else
                            editCompany({ id: editId, ...data }, logoFile).then(() => {
                                this.loadCompanies();
                                history.push('/companies');
                            });
                    }}
                />
            </Modal>
        </PageController>;
    }

}