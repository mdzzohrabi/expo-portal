import { Badge, Button, Descriptions, Input, Select, Switch, Icon, Avatar, message, AutoComplete } from "antd";
import * as React from 'react';
import { useEffect, useState } from "react";
import { RouteComponentProps } from "react-router-dom";
import { assetUrl, CompanyApi, ProductApi, upload, CategoryApi } from "../../api";
import { trans } from "../../strings";
import { Product } from "../../typeDefs";
import { useAsyncMemo, useInputChange, intersectKey } from "../../util";
import { PageController } from "./pageController";
import { MediaManager } from "../mediaManager";
import { LoginController } from "./login";

/**
 * Product controller page
 */
export function ProductController(props: RouteComponentProps) {

    let queryId = props.match.params['id'];

    let isNew = queryId == 'add';
    let [loading, setLoading] = useState(false);
    let [product, setProduct] = useState<Product>({});
    let [formData, setFormData, handleInputChange] = useInputChange<Product>({});
    let [companyFilter, setCompanyFilter] = useState<string>();
    let [afterSubmit, setAfterSubmit] = useState<Function[]>([]);
    let [companies, companiesLoading] = useAsyncMemo([], () => CompanyApi.list(companyFilter || formData?.company?.name), [companyFilter, formData?.company?.name]);
    let [poster, setPoster] = useState();
    let [categories, categoriesLoading] = useAsyncMemo([], () => CategoryApi.list(), []);
    let [edit, setEdit] = useState(isNew);

    // Load product
    useEffect(() => {
        if (queryId != 'add') {
            setLoading(true);
            ProductApi.get(queryId).then(response => {
                setProduct(response.data);
                setLoading(false);
            });
        }
    }, [queryId]);

    // Reset form data on etering edit mode
    useEffect(() => {
        setFormData(product);
    }, [edit]);

    let editProduct = React.useCallback(() => {
        let loadingNotify = message.loading(trans('Please wait ...'));
        let data = intersectKey(formData, ['id', 'name', 'description', 'company', 'active', 'category']);
        ProductApi.edit(data).then(result => {
            Promise.all(afterSubmit.map(callback => callback(result.data))).then(() => {
                loadingNotify();
                props.history.push('/products');
            })
        }).catch(loadingNotify);
    }, [formData, afterSubmit]);

    let addProduct = React.useCallback(() => {
        let loadingNotify = message.loading(trans('Please wait ...'));
        let data = intersectKey(formData, ['name', 'description', 'company', 'active', 'category', 'users']);
            data.users = [LoginController.User.id as any];
        ProductApi.new(data).then(result => {
            Promise.all(afterSubmit.map(callback => callback(result.data))).then(() => {
                loadingNotify();
                props.history.push('/products');
            })
        }).catch(loadingNotify);
    }, [formData, afterSubmit]);

    let changePoster = React.useCallback(() => {
        let file = document.createElement('input');
            file.type = 'file';
            file.onchange = () => {
                if (file.files.length > 0) {
                    let reader = new FileReader();
                        reader.readAsDataURL(file.files[0]);
                        reader.onload = e => setPoster(e.target.result);
                    setAfterSubmit([
                        (product: Product) => {
                            return upload(file.files.item(0), 'product', product.id, 'poster');
                        }
                    ])
                }        
            }
            file.click();
    }, []);

    return <PageController
        loading={loading}
        avatar={{ src: assetUrl(product.poster?.url ?? '/uploads/d5132890833647e8be2dccfb8e561a55.png') }}
        title={trans("Product")}
        subTitle={ loading ? "Loading..." : product.name }
        extra={[
            isNew ?
                <Button key="submit" onClick={() => addProduct()} type="primary" icon="check">{trans('Add product')}</Button>
            : (
            !edit ? <Button type="primary" key="edit" onClick={() => setEdit(true)}>{trans('Edit product')}</Button> : <Button.Group key="actions" className="ltr">
                <Button key="cancel" onClick={() => setEdit(false)} icon="close">{trans('Cancel')}</Button>
                <Button key="submit" onClick={() => editProduct()} type="primary" icon="check">{trans('Submit changes')}</Button>
            </Button.Group> )
        ]}
        {...props}>
        <Descriptions title={product.name} bordered>
            <Descriptions.Item label={trans("Name")}>
                { edit ? <Input name="name" type="text" value={formData.name} onChange={handleInputChange}/> :  product.name }
            </Descriptions.Item>
            <Descriptions.Item label={trans("Category")}>
                { edit ? <AutoComplete dataSource={categories.map(c => c.name)} value={formData.category} onChange={value => setFormData({ ...formData, category: value.toString() })}/> : product.category}
            </Descriptions.Item>
            <Descriptions.Item label={trans("Status")}>
                {
                    edit ? <Switch onChange={checked => handleInputChange({ currentTarget: { name: 'active', value: checked } } as any)} checked={formData.active}/> : <React.Fragment><Badge key="active-badge" status={product.active ? "processing" : "error"}/><span key="active-text">{trans(product.active ? "Active" : "De-active")}</span></React.Fragment>
                }
            </Descriptions.Item>
            <Descriptions.Item label={trans("Company")}>
                { edit ? <Select
                    showSearch
                    optionFilterProp="children"
                    onSearch={q => setCompanyFilter(q)}
                    style={{ width: 350 }}
                    filterOption={true}
                    loading={companiesLoading}
                    value={formData.company?.id}
                    suffixIcon={<img src={assetUrl(formData.company?.logo?.url)} style={{
                        width: 40,
                        border: '1px #bfbfbf solid',
                        borderRadius: '100%',
                        marginTop: -13,
                        marginRight: -20,
                        height: 40,
                        overflow: 'hidden'
                    }}/>}
                    onChange={selectedId => setFormData({ ...formData, company: companies.find(c => c.id == selectedId) })}>
                    {companies.map(company => <Select.Option key={company.id}>{company.name}</Select.Option>)}
                </Select> : product.company?.name ?? '-'}
            </Descriptions.Item>
            <Descriptions.Item label={trans("Description")} span={2}>
                { edit ? <Input.TextArea name="description" value={formData.description} onChange={handleInputChange}/> : product.description}
            </Descriptions.Item>
            { edit ? 
            <Descriptions.Item label={trans("Poster")}>
                <Avatar size={100} src={poster || assetUrl(product.poster?.url ?? '/uploads/d5132890833647e8be2dccfb8e561a55.png')}/> <Button onClick={changePoster}>{trans("Change image ...")}</Button>
            </Descriptions.Item> : null }
        </Descriptions>
        { !isNew ? <MediaManager upload={edit} modelType="product" modelId={product.id} mediaType="medias"/> : null }
    </PageController>

}