import * as React from 'react';

export interface ShowWhenProps {
    expr: boolean
    children?: any
    otherwise?: JSX.Element
}

export function ShowWhen({ expr, children, otherwise }: ShowWhenProps) {
    if (expr)
        return children;
    return otherwise || null;
}