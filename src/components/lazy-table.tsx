import { Table } from "antd";
import { PaginationConfig, TableProps } from "antd/lib/table";
import { parse as parseQuery } from 'qs';
import { useCallback, useEffect, useState } from "react";
import { RouteComponentProps } from "react-router-dom";
import * as React from 'react';


export interface LazyTableProps<T> extends RouteComponentProps, Omit<TableProps<T>, 'dataSource'> {
    limit?: number
    totalSuffix?: any
    basePath: string
    pageSizes?: string[]
    dataSource: (filter: any, start: number, limit: number) => Promise<{ values: T[], aggregate: { totalCount: number } }>
}

export function LazyTable<T>(props: LazyTableProps<T>) {
    
    let [data, setData] = useState<T[]>([]);
    let [loading, setLoading] = useState(true);
    let [page, setPage] = useState(1);
    let [limit, setLimit] = useState(props.limit || 20);
    let [pagination, setPagination] = useState({
        current: page,
        pageSize: limit,
        simple: false,
        pageSizeOptions: props.pageSizes || ['20', '50', '100'],
        showSizeChanger: true,
        showTotal: (total, range) => <span>{ total } { props.totalSuffix }</span>,
        ...props.pagination
    });
    let {history, location, match,dataSource, ...attrs} = props;
    let { page: queryPage, size: queryLimit } = parseQuery(location.search.substr(1));

    let changePage = useCallback((pager: PaginationConfig, filters: any) => {
        history.push(`${props.basePath}?page=${pager.current}&size=${pager.pageSize}`);
    }, [history]);

    let defaultRowKey = useCallback((row: any) => row.id, []);

    // Update state from query string
    useEffect(() => {
        setPage(Number(queryPage) || 1);
        setLimit(Number(queryLimit) || limit);
    }, [queryPage, queryLimit]);

    useEffect(() => {
        setLoading(true);
        props.dataSource({}, (page - 1) * limit, limit).then(result => {
            setData(result.values);
            setLoading(false);
            setPagination({
                ...pagination,
                current: page,
                pageSize: limit,
                total: result.aggregate.totalCount
            });
        });
    }, [page, limit]);

    return <Table className="ltr" dataSource={data} loading={loading} pagination={pagination} onChange={changePage} rowKey={defaultRowKey} {...attrs}/>

}