import * as React from "react";
import { Icon } from 'antd';

export class ErrorHandler extends React.Component {

    state = { hasError: false }
  
    static getDerivedStateFromError(error) {
      // Update state so the next render will show the fallback UI.
      return { hasError: true };
    }
  
    componentDidCatch(error, info) {
      // You can also log the error to an error reporting service
      console.error(error);
    }
  
    render() {
      if (this.state.hasError) {
        // You can render any custom fallback UI
        return <div>
            <div style={{ textAlign: 'center', marginTop: 40, marginBottom: 10 }}><Icon style={{ fontSize: '80px',textAlign: 'center' }} theme="twoTone" type="meh"/></div>
            <h1 style={{ textAlign: 'center', color: '#aaa', fontWeight: 100 }}>An exception occured</h1>
        </div>;
      }
  
      return this.props.children; 
    }
  }