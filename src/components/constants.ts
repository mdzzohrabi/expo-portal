export const VERSION = '1.1.0';
export const APP_NAME = 'portal.web';

export const STORAGE_CONFIG = 'config';
export const STORAGE_VERSION = 'version';
export const DEBUG = process.env.NODE_ENV == 'development';

export const DEFAULT_CONFIG = {
    imageWidth: 1600
};