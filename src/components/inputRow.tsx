import { Row, Col, Input } from "antd";
import strings from "../strings";
import * as React from 'react';

export interface InputRowProps {
    label: string
    children?: any
}

export function InputRow({ label, children }: InputRowProps) {
    return <Row className="mb input rtl">
        <Col span={ 24 } md={ 6 }><label>{ label }</label></Col>
        <Col span={ 24 } md={ 18 }>
            { children }
        </Col>
    </Row>
}