import { RouteProps, Route, Redirect } from "react-router-dom";
import * as React from 'react';
import { LoginController } from "./controller/login";

export function SecureRoute({ component, ...rest }: RouteProps) {

    return <Route
        { ...rest }
        render={props => {
            return LoginController.isAuthenticated ? React.createElement(component, props) : <Redirect to={{ pathname: '/login', state: { from: props.location } }}/>;
        }}
    />

}