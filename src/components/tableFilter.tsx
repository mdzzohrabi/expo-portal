import { Input, Button, Icon } from "antd";
import * as React from 'react';
import { trans } from "../strings";

export function tableFilter(_this, dataIndex) {
    let searchInput;

    let handleSearch = (selectedKeys, confirm) => {
        confirm();
        _this.setState({ searchText: selectedKeys[0] });
    };
    
    let handleReset = clearFilters => {
        clearFilters();
        _this.setState({ searchText: '' });
    };

    return ({
        filterDropdown: ({ setSelectedKeys, selectedKeys, confirm, clearFilters }) => (
            <div style={{ padding: 8 }}>
                <Input
                    ref={node => searchInput = node}
                    placeholder={`Search ${dataIndex}`}
                    value={selectedKeys[0]}
                    onChange={e => setSelectedKeys(e.target.value ? [e.target.value] : [])}
                    onPressEnter={() => handleSearch(selectedKeys, confirm)}
                    style={{ width: 188, marginBottom: 8, display: 'block' }}
                />
                <Button
                    type="primary"
                    onClick={() => handleSearch(selectedKeys, confirm)}
                    icon="search"
                    size="small"
                    style={{ width: 90, marginRight: 8 }}
                >{ trans('Search') }</Button>
                <Button onClick={() => handleReset(clearFilters)} size="small" style={{ width: 90 }}>{ trans('Reset') }</Button>
            </div>
        ),

        filterIcon: filtered => (
            <Icon type="search" style={{ color: filtered ? '#1890ff' : undefined }} />
        ),

        onFilter: (value, record) => {
            return JSON.stringify(record[dataIndex] || '')
                .toLowerCase()
                .includes(value.toLowerCase())
        },

        onFilterDropdownVisibleChange: visible => {
            if (visible) {
                setTimeout(() => searchInput.select());
            }
        }
  });

}