import { Button, Col, Dropdown, Icon, Menu, message, Modal, Progress, Row, Switch, Table, Upload, Popconfirm } from "antd";
import TextArea from "antd/lib/input/TextArea";
import { UploadChangeParam } from "antd/lib/upload/interface";
import * as React from "react";
import { approveMedia, assetUrl, medias, newMedia, deleteMedia, getConfig } from "../api";
import { trans } from "../strings";
import { ContentMedia } from "../typeDefs";
import { resizeImage, Sorter, isVideo } from "../util";
import { LoginController } from './controller/login';
import { ShowWhen } from "./showWhen";
import { tableFilter } from "./tableFilter";
import Group from "antd/lib/input/Group";
import { PaginationConfig, ColumnProps } from "antd/lib/table";
export interface MediaManagerProps {
    modelType?: 'expo' | 'company' | 'product' | 'stand'
    modelId?: string
    mediaType?: string
    upload?: boolean
    onUpload?: (data: any) => any
}

export interface UploadItem extends File {
    image: string
    uploaded: number
    uid: string
}

export class MediaManager extends React.Component<MediaManagerProps> {

    state = {
        // Clicked Media (Modal)
        media: null,
        // Table Columns
        columns: [
            { title: trans('Media'), dataIndex: 'file.url', key: 'file', render: this.renderMedia.bind(this) },
            { title: trans('Size'), dataIndex: 'file.size', key: 'size', sorter: (a, b) => Sorter.Number('size')(a.file, b.file) },
            { title: trans('Visits'), dataIndex: 'visits', key: 'visits' },
            { title: trans('Likes'), dataIndex: 'likes', key: 'likes' },
            { title: trans('User'), dataIndex: 'user.username', key: 'user', ...tableFilter(this, 'user') },
            { title: trans('Description'), dataIndex: 'description', key: 'description', ...tableFilter(this, 'description') },
            { title: trans('Approved'), dataIndex: 'approved', key: 'approved', render: this.renderApproved.bind(this) },
            { title: trans('Approved On'), dataIndex: 'approvedOn', key: 'approvedOn', sorter: Sorter.Date('approvedOn') },
            { title: trans('Created On'), dataIndex: 'createdAt', key: 'createdAt', sorter: Sorter.Date('createdAt') }
        ] as ColumnProps<any>[],
        // Medias
        medias: [] as ContentMedia[],
        // Loading
        loading: true,
        // Upload Queue
        uploads: [] as UploadItem[],
        // Upload Text
        text: '',
        selectedRows: [] as string[],
        pagination: {
            showSizeChanger: true,
            defaultPageSize: 5,
            pageSizeOptions: ['5', '10', '20', '30', '50', '100']
        } as PaginationConfig
    }

    async componentDidMount() {
        await this.loadMedias();
        if ( !this.props.modelType ) {
            this.setState({
                columns: [
                    ...this.state.columns,
                    {
                        title: trans('Expo'), key: 'expo', dataIndex: 'expo.title',
                        render: (value, media: ContentMedia) => <span>{ ( media.expo || (media.stand && media.stand.expo) || { title: null } ).title }</span>
                    },
                    { title: trans('Stand'), key: 'stand', dataIndex: 'stand.name' },
                    { title: trans(`Company`), key: 'company', dataIndex: 'stand.company.name' }
                ]
            })
        }
    }

    async loadMedias(start = 0, limit = 5) {
        let { modelId, modelType, mediaType } = this.props;
        let pagination = { ...this.state.pagination };
        this.setState({ loading: true });
        let result = await medias(modelType, modelId, mediaType, start, limit);
        pagination.total = result.aggregate.totalCount;
        this.setState({ loading: false, medias: result.values, pagination });
    }

    renderMedia(url: string, media: any) {
        return <div style={{ position: 'relative', textAlign: 'center' }}>
            <img onClick={() => this.setState({ media: assetUrl(url) })} src={ assetUrl(url, 80) } style={{ width: 80, cursor: 'pointer' }}/>
            { isVideo(url) ? 
            <Icon type={ isVideo(url) ? "video-camera" : "picture" } style={{
                position: 'absolute',
                left: 'calc(50% - 15px )', 
                top: 'calc(50% - 15px)', 
                fontSize: '15px',
                color: 'white',
                background: '#000000a3',
                padding: '5px',
                borderRadius: '50%'
            }}/> : null }
        </div>
    }

    renderApproved(approved: boolean, media: ContentMedia) {
        return <Switch disabled={ !LoginController.isAdmin } checked={ approved } loading={ media.approvedLoading } onChange={this.updateApproved.bind(this, media)}/>
    }

    async updateApproved(media: ContentMedia, approved: boolean) {
        let index = this.state.medias.indexOf(media);
        let medias = [ ...this.state.medias ];
        medias[index].approvedLoading = true;
        this.setState({ medias });
        await approveMedia(media.id, approved);
        this.loadMedias();
    }

    async uploadFile({ file: mainFile, fileList }: UploadChangeParam<File>) {

        let { modelType, modelId, mediaType } = this.props;
        let { text } = this.state;

        let doUpload = (file: File) => {
            let reader = new FileReader();
            reader.onload = (e) => {
                let uid = (file as any).uid as string;
    
                this.setState({
                    uploads: [ ...this.state.uploads, { image: (e.target as any).result, ...file, uploaded: 0 } ]
                }, () => {
                    newMedia(file, mediaType, modelType, modelId, text,
                        progress => {
                            let uploads = [ ... this.state.uploads.map(upload => {
                                if (upload.uid == uid) {
                                    upload.uploaded = progress.loaded;
                                    upload.size = progress.total;
                                }
                                return upload;
                            }) ];
                            this.setState({ uploads });
                    })
                    .then(r => {
                        message.success(trans('Media uploaded successfull'));
                        this.loadMedias();
                        if (this.props.onUpload)
                            this.props.onUpload(r);
                    })
                    .catch(e => {
                        message.error(trans(`Issue during media upload`));
                    });
                });
            }
            reader.readAsDataURL(file);
        }

        // Resize image
        if ( mainFile.type.match(/image.*/) ) {
            resizeImage({ file: mainFile, maxSize: (await getConfig()).imageWidth }).then(result => {
                let file = new File([ result ], mainFile.name, {
                    lastModified: mainFile.lastModified,
                    type: mainFile.type
                });

                doUpload(file);
            });
        } else {
            doUpload(mainFile);
        }
    }

    async approveSelected({ key }) {
        let { selectedRows, medias } = this.state;
        medias = medias.map(media => {
            if ( selectedRows.includes(media.id) ) media.approvedLoading = true;
            return media;
        });
        this.setState({ medias });
        await Promise.all( selectedRows.map(rowId => approveMedia(rowId, key == 'true')) );
        this.loadMedias();
    }

    async deleteSelected() {
        let { selectedRows, medias } = this.state;
        let loading = message.loading(trans('Deleting ...'))
        await Promise.all( selectedRows.map(rowId => deleteMedia(rowId) ));
        loading();
        this.loadMedias();
    }

    render() {

        let { upload } = this.props;
        let { columns, medias, media, uploads, loading, pagination } = this.state;

        return <React.Fragment>
            <Table
                className="ltr"
                dataSource={ medias }
                columns={ columns }
                rowKey={r => r.id}
                loading={ loading }
                rowSelection={{
                    type: 'checkbox',
                    onChange: (keys) => this.setState({ selectedRows: keys })
                }}
                pagination={pagination}
                onChange={(_pagination, filters, sorter) => {

                    let pager = { ...pagination };
                    pager.current = _pagination.current;
                    this.setState({ pagination: pager });
                    this.loadMedias( (_pagination.current - 1) * _pagination.pageSize, _pagination.pageSize );

                }}
                footer={(data) => <div>
                    { LoginController.hasAcl('portal.approveMedia') ? <Dropdown overlay={<Menu onClick={this.approveSelected.bind(this)}>
                        <Menu.Item key={'true'}>{ trans('Approve') }</Menu.Item>
                        <Menu.Item key={'false'}>{ trans('Dis-Approve') }</Menu.Item>
                    </Menu>}>
                        <Button>{ trans('Approve/DisApprove') } <Icon type={'down'}/></Button>
                    </Dropdown> : null }
                    &nbsp;
                    { LoginController.isAdmin || LoginController.hasAcl('portal.deleteMedia') ? <Popconfirm
                        title={trans(`Are you sure to delete selected items ?`)}
                        okText={trans('Yes, Delete')}
                        cancelText={trans('Cancel')}
                        onConfirm={this.deleteSelected.bind(this)}
                    >
                        <Button type={'danger'} icon={'delete'}>{ trans('Delete') }</Button>
                    </Popconfirm>  : '' }
                </div>}
            />
            <Modal
                visible={ !!media }
                title={ trans('Image') }
                maskClosable={true}
                mask={true}
                centered={true}
                width={"auto"}
                footer={ <Button type="default" onClick={() => this.setState({ media: null })}>{ trans('Close') }</Button> }
                onCancel={() => this.setState({ media: null })}
            >
                { media ? ( isVideo(media) ? <video controls autoPlay src={ media } style={{ width: '100%' }}/> : <img src={ media } style={{ width:'100%' }}/> ) : null }
            </Modal>
            <ShowWhen expr={upload}>
                { uploads.map(upload => {
                    return <Row key={upload.uid || Math.random()} style={{ marginBottom: 10 }}>
                        <Col span={20}>
                            <Progress percent={ Math.round( upload.uploaded / upload.size * 100 ) }/>
                        </Col>
                        <Col span={4} style={{ textAlign: 'center' }}><img src={upload.image} style={{ height: 25 }}/></Col>
                    </Row>
                }) }
                {/* <input type="file" onChange={(e) => {
                    this.uploadFile({ file: e.target.files.item(0), fileList: null });
                }}/> */}
                <Upload.Dragger
                    name="media"
                    multiple={true}
                    beforeUpload={file => {
                        return false;
                    }}
                    showUploadList={false}
                    listType="picture-card"
                    onChange={this.uploadFile.bind(this)}
                    className="clearfix"
                    accept="image/*,.mp4,.webm"
                    >
                    <Icon type="cloud-upload"/>
                    <p>{ trans('Click or drag files here') }</p>
                </Upload.Dragger>
                <TextArea placeholder={trans('Upload Description')} value={ this.state.text } onChange={event => this.setState({ text: event.target.value })} style={{ marginTop: 10 }}/>
            </ShowWhen>
        </React.Fragment>
    }

}