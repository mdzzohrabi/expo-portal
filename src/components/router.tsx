import * as React from 'react';
import { BrowserRouter, Route, Redirect, Switch } from 'react-router-dom';
import { LoginController } from './controller/login';
import { SecureRoute } from './SecureRoute';
import { ExposPage } from './controller/expos';
import { ExpoController } from './controller/expo';
import { StandController } from './controller/stand';
import { MediasController } from './controller/medias';
import { ErrorHandler } from './errorHandler';
import { CompaniesController } from './controller/companies';
import { StoresController } from './controller/stores';
import { ProductsController } from './controller/products';
import { ProductController } from './controller/product';
import { LocaleProvider } from '../provider/locale-provider';
import { getLocale, setLocale } from '../strings';
import { StoreProductsController } from './controller/store_products';

export function AppRouter() {

    let [lang, setLang] = React.useState(getLocale());

    setLocale(lang);

    return <ErrorHandler>
        <LocaleProvider.Provider value={{ lang, setLang }}>
        <BrowserRouter>
            <Switch>
                <Route path="/login" exact component={ LoginController }/>
                <Route path="/logout" exact component={ LoginController }/>
                <SecureRoute path="/expos" component={ ExposPage }/>
                <SecureRoute path="/expos/add" component={ ExpoController }/>
                <SecureRoute path="/expo/:id" component={ ExpoController }/>
                <SecureRoute path="/stand/:standId" component={ StandController }/>
                <SecureRoute path="/medias" exact component={ MediasController }/>
                <SecureRoute path="/companies" exact component={ CompaniesController }/>
                <SecureRoute path="/stores" exact component={ StoresController }/>
                <SecureRoute path="/products/:id" exact component={ StoreProductsController }/>
                <SecureRoute path="/products" exact component={ ProductsController }/>
                <SecureRoute path="/product/:id" exact component={ ProductController }/>
                <Redirect path="/" to="/expos"/>
            </Switch>
        </BrowserRouter>
        </LocaleProvider.Provider>
    </ErrorHandler>
}