import * as React from "react";

export class ScrollIntoView extends React.Component<{ id: string }> {

	componentDidMount() {
	  this.scroll()
	}
  
	componentDidUpdate() {
	  this.scroll()
	}
  
	scroll() {
	  const { id } = this.props
	  if (!id) {
		return
	  }
	  const element = document.querySelector(id)
	  if (element) {
		element.scrollIntoView()
	  }
	}
  
	render() {
	  return this.props.children;
	}
}