import { Icon, Menu } from 'antd';
import * as React from 'react';
import { Link } from 'react-router-dom';
import { trans } from '../strings';
import { LoginController } from './controller/login';
import { LocaleProvider } from '../provider/locale-provider';

export function PanelMenu() {
    return <LocaleProvider.Consumer>{({lang, setLang}) => {
        return <Menu mode="horizontal" theme="light">
            { LoginController.hasAcl('portal.expos') ? <Menu.Item>
                <Link to="/expos"><Icon type="windows"/>{ trans('Expos') }</Link>
            </Menu.Item> : null }
            { LoginController.isAdmin || LoginController.hasAcl('portal.medias') ? 
                <Menu.Item>
                    <Link to="/medias"><Icon type="picture"/>{ trans('Medias') }</Link>
                </Menu.Item> : null }
            { LoginController.hasAcl('portal.companies') ? 
                <Menu.Item>
                    <Link to="/companies"><Icon type="apartment"/>{ trans('Companies') }</Link>
                </Menu.Item> : null }
            { LoginController.hasAcl('portal.stores') ? 
                <Menu.Item>
                    <Link to="/stores"><Icon type="shop"/>{ trans('Stores') }</Link>
                </Menu.Item> : null }
            { LoginController.hasAcl('portal.products') ? 
                <Menu.Item>
                    <Link to="/products"><Icon type="shopping-cart"/>{ trans('Products') }</Link>
                </Menu.Item> : null }
            <Menu.Item>
                <Link to="/logout"><Icon type="logout"/>{ trans('Logout') }</Link>
            </Menu.Item>
            <Menu.SubMenu title={<span><Icon type="global"/> {trans('Language')} ({trans('language_name')})</span>}>
                <Menu.Item onClick={() => setLang('fa')}>{trans('Persian')}</Menu.Item>
                <Menu.Item onClick={() => setLang('en')}>{trans('English')}</Menu.Item>
            </Menu.SubMenu>
        </Menu>;
    }}</LocaleProvider.Consumer>
}