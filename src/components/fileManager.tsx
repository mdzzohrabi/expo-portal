import { Button, Col, Icon, message, Modal, Progress, Row, Table, Upload } from "antd";
import { UploadChangeParam } from "antd/lib/upload/interface";
import * as React from "react";
import { assetUrl, upload } from "../api";
import { trans } from "../strings";
import { resizeImage } from "../util";
import { ShowWhen } from "./showWhen";
import { tableFilter } from "./tableFilter";

export interface FileManagerProps {
    medias: any[]
    uploadRef?: string
    uploadRefId?: string
    upload?: boolean
    onUpload?: (data: any) => any
}

export interface UploadItem extends File {
    image: string
    uploaded: number
    uid: string
}

export class FileManager extends React.Component<FileManagerProps> {

    state = {
        media: null,
        columns: [
            { title: 'Media', dataIndex: 'url', key: 'url', render: this.renderMedia.bind(this) },
            { title: 'Name', dataIndex: 'name', key: 'name', ...tableFilter(this, 'name') },
            { title: 'Size', dataIndex: 'size', key: 'size' },
            { title: 'Mime', dataIndex: 'mime', key: 'mime' },
            { title: 'Ext', dataIndex: 'ext', key: 'ext' },
            { title: 'Created On', dataIndex: 'createdAt', key: 'createdAt', sorter: (a, b) => new Date(a.createdAt) > new Date(b.createdAt) ? 1 : -1 }
        ],
        uploads: [] as UploadItem[]
    }

    renderMedia(url: string, media: any) {
        return <img onClick={() => this.setState({ media: assetUrl(url) })} src={ assetUrl(url, 80) } style={{ width: 80, cursor: 'pointer' }}/>;
    }

    uploadFile({ file: mainFile, fileList }: UploadChangeParam<File>) {

        resizeImage({ file: mainFile, maxSize: 1024 }).then(result => {

            let file = new File([ result ], mainFile.name, {
                lastModified: mainFile.lastModified,
                type: mainFile.type
            });

            let reader = new FileReader();
            let { uploadRef: ref, uploadRefId: refId } = this.props;
            reader.onload = (e) => {
                let uid = (file as any).uid as string;
    
                this.setState({
                    uploads: [ ...this.state.uploads, { image: (e.target as any).result, ...file, uploaded: 0 } ]
                }, () => {
                    upload(file, ref, refId, 'images',
                        progress => {
                            let uploads = [ ... this.state.uploads.map(upload => {
                                if (upload.uid == uid) {
                                    upload.uploaded = progress.loaded;
                                    upload.size = progress.total;
                                }
                                return upload;
                            }) ];
                            this.setState({ uploads });
                    })
                    .then(r => {
                        message.success(trans('Media uploaded successfull'));
                        if (this.props.onUpload)
                            this.props.onUpload(r);
                    })
                    .catch(e => {
                        message.error(trans(`Issue during media upload`));
                    });
                });
            }
            reader.readAsDataURL(file);    

        });
    }

    render() {

        let { medias, upload } = this.props;
        let { columns, media, uploads } = this.state;

        return <React.Fragment>
            <Table
                className="ltr"
                dataSource={ medias }
                columns={ columns }
                rowKey={r => r.id}
            />
            <Modal
                visible={ !!media }
                title={ trans('Image') }
                maskClosable={true}
                mask={true}
                centered={true}
                width={"auto"}
                footer={ <Button type="default" onClick={() => this.setState({ media: null })}>{ trans('Close') }</Button> }
                onCancel={() => this.setState({ media: null })}
            >
                { media ? <img src={ media } style={{ width:'100%' }}/> : null }
            </Modal>
            <ShowWhen expr={upload}>
                { uploads.map(upload => {
                    return <Row key={upload.uid || Math.random()} style={{ marginBottom: 10 }}>
                        <Col span={20}>
                            <Progress percent={ Math.round( upload.uploaded / upload.size * 100 ) }/>
                        </Col>
                        <Col span={4} style={{ textAlign: 'center' }}><img src={upload.image} style={{ height: 25 }}/></Col>
                    </Row>
                }) }
                {/* <input type="file" onChange={(e) => {
                    this.uploadFile({ file: e.target.files.item(0), fileList: null });
                }}/> */}
                <Upload.Dragger
                    name="media"
                    multiple={true}
                    beforeUpload={file => {
                        return false;
                    }}
                    showUploadList={false}
                    listType="picture-card"
                    onChange={this.uploadFile.bind(this)}
                    className="clearfix"
                    accept="image/*"
                    >
                    <Icon type="cloud-upload"/>
                    <p>{ trans('Click or drag files here') }</p>
                </Upload.Dragger>
            </ShowWhen>
        </React.Fragment>
    }

}