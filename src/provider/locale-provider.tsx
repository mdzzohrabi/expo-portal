import {getLocale, setLocale} from "../strings";
import React from "react";

export const LocaleProvider = React.createContext({
    lang: getLocale(),
    setLang: setLocale
});