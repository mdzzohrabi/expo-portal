import axios from 'axios';
import {gql, ApolloClient} from 'apollo-boost';
import { createUploadLink } from 'apollo-upload-client';
import { InMemoryCache, StoreReader } from 'apollo-cache-inmemory';
import invariant from 'ts-invariant';
import { Expo, Stand, ContentMedia, Company, Store, Product, ProductPrice } from './typeDefs';
import { LoginController } from './components/controller/login';
import { isVideo } from './util';
import { APP_NAME, STORAGE_CONFIG, DEFAULT_CONFIG } from './components/constants';

export let cdnUrl = "http://media.beshenas.com/?u=";
let apiHost = 'http://api.beshenas.com';

let client: ApolloClient<any>; 

export function assetUrl(assetPath: string, width?: number, height?: number) {
    return cdnUrl + assetPath + ( width ? '&w=' + width : '' ) + ( height ? '&h=' + height : '' ) + (width && isVideo(assetPath) ? '&thumb=1' : '');
}

function parepareWhere(where: any = {}) {
    let whereStr = [];
    for (let key in where) {
        if (Array.isArray(where[key]))
            whereStr.push(`${key}: [${where[key].map(item => '"' + item + '"')}]`);
        else
            whereStr.push(`${key}: "${where[key]}"`);
    }
    return whereStr.join(', ');
}

/**
 * Make an api request
 * @param method Request method
 * @param endPoint End-point
 * @param data Request data
 */
export function request(method: 'post' | 'get' | 'put' | 'delete', endPoint: string, data?: any) {
    return axios.request({
        method,
        url: apiHost + endPoint,
        data,
        headers: (() => {
            let headers = {
                'Content-Type': 'application/json'
            }

            if (LoginController.isAuthenticated)
                headers['Authorization'] = `Bearer ${ LoginController.StoredToken }`;

            return headers;
        })()
    })
}

export function createClient(token: string) {
    client = new ApolloClient({
        link: createUploadLink({
            uri: `${ apiHost }/graphql`,
            headers: {
                Authorization: `Bearer ${ token }`
            }
        }),
        cache: new InMemoryCache(),
        defaultOptions: {
            query: {
                fetchPolicy: 'no-cache'
            }
        }
    });
}

export async function login(username, password) {
    return await request('post', '/auth/local', {
        identifier: username,
        password
    }).then(response => response.data);
}

export function expos(): Promise<Expo[]> {
    invariant(client, `Apollo client not created yet`);
    return client.query({
        query: gql`query Expos($user: ID) {
            expos(where: { operators: $user }) { id title country province city category }
        }`,
        variables: { user: LoginController.User.id }
    })
    .then(r => r.data.expos);
}

export function expo(id: string): Promise<Expo> {
    invariant(client, `Apollo client not created yet`);
    return client.query({
        query: gql`query Expo ($id: ID!) {
            expo(id: $id) { id title country province city category startTime endTime poster { url } address description category images { url hash id name size mime ext createdAt } }
        }`,
        variables: { id }
    })
    .then(r => r.data.expo);
}

export function stands(id: string, start?: number, limit?: number, where?: any): Promise<{ values: Stand[], aggregate: { totalCount: number } }> {
    invariant(client, `Apollo client not created yet`);
    where = where || {};

    if (!LoginController.hasAcl('portal.allStands')) {
        where['users'] = LoginController.User.id;
    }
    
    return client.query({
        query: gql`query Expo ($start: Int, $limit: Int) {
            stands: standsConnection(where: {${ parepareWhere({ expo: id, ...where }) }}, start: $start, limit: $limit) {
                values {
                    id name standNo cover {url} address introVideo { url } company { id name }
                }
                aggregate { totalCount: count }
            }
        }`,
        variables: { start, limit }
    })
    .then(r => r.data.stands);

}

export function stand(id: string): Promise<Stand> {
    invariant(client, `Apollo client not created yet`);
    return client.query({
        query: gql`query Stand ($id: ID!) {
            stand(id: $id) { id name standNo address description images { url hash id name size mime ext createdAt } }
        }`,
        variables: { id }
    })
    .then(r => r.data.stand);
}

export function medias(modelName: string, modelId: string, mediaType: string, from: number, limit: number): Promise<{ values: ContentMedia[], aggregate: { totalCount: number } }> {
    invariant(client, `Apollo client not created yet`);

    let selectQuery = `
    values {
        id file { mime ext url size } description user { username } createdAt updatedAt approved visits likes stand { id name expo { id title } company { id name } } expo { id title }
    }
    aggregate { totalCount: count }
    `;
    
    if (LoginController.isAdmin) {

        // All medias
        if (!modelName) {
            return client.query({
                query: gql`query AllMedias {
                    medias: contentmediasConnection(sort: "createdAt:desc", start: ${ from }, limit: ${ limit }) { ${selectQuery} }
                }`
            })
            .then(r => r.data.medias);
        }

        // Specific content
        return client.query({
            query: gql`query Medias ($where: JSON) {
                medias: contentmediasConnection(where: $where, sort: "createdAt:desc", start: ${ from }, limit: ${ limit }) { ${selectQuery} }
            }`,
            variables: { 
                where: {
                    [modelName]: modelId,
                    mediaType,
                    user: LoginController.User.id
                }
            }
        })
        .then(r => r.data.medias);
    }
    else {

        // User medias only for specific content
        return client.query({
            query: gql`query Medias ($where: JSON) {
                medias: contentmediasConnection(where: $where, sort: "createdAt:desc", start: ${ from }, limit: ${ limit }) { ${selectQuery} }
            }`,
            variables: {
                where: {
                    [modelName]: modelId,
                    mediaType,
                    user: LoginController.User.id
                }
            }
        })
        .then(r => r.data.medias);
    }
}

export function fetchCompaniesGrid(start: number = 0, limit: number = 10, filter: any = {}): Promise<{ values: Company[], aggregate: { totalCount: number }}> {
    invariant(client, `Apollo client not created yet`);
    return client.query<{
        companies: {
            values: Company[],
            aggregate: { totalCount: number }
        }
    }>({
        query: gql`
        query CompaniesGrid($start: Int, $limit: Int) {
            companies: companiesConnection(start: $start, limit: $limit, where: { ${ parepareWhere( filter ) } }) {
                values {
                id name
                logo { url }
                description
                longDescription
                type
                showInCompanies
                email
                website
                phones
                address
                province
                country
                city
                tags { name }
                }
                aggregate { totalCount: count }
            }
        }`,
        variables: { start, limit }
    })
    .then(r => r.data.companies);

}


export function companies(filter?: string, start: number = 0, limit: number = 100): Promise<Company[]> {
    let companies = LoginController.User?.companies?.map(c => c.id) ?? [];
    let companiesFilter = companies.length > 0 ? '_id_in[]=' + companies.join('&_id_in[]=') : '';
        filter = filter ? 'name_contains=' + filter : '';
    return request('get', `/companies?${ [filter, companiesFilter].filter(a => a.length > 0).join('&') }`).then(result => result.data);
}

export async function editCompany(company: Company, logo?: File, onProgress?: (event: any) => any) {
    let result = await request('put', `/companies/${ company.id }`, company);
    if ( logo )
        return upload(logo, 'company', result.data.id, 'logo', onProgress);
    return result;
}

export async function newCompany(company: Company, logo?: File, onProgress?: (event: any) => any) {
    let result = await request('post', `/companies`, company);
    if ( logo )
        return upload(logo, 'company', result.data.id, 'logo', onProgress);
    return result;
}

export async function newMedia(file: File, mediaType: string | undefined | null, modelName: 'expo' | 'stand' | 'company' | 'product', modelId: string, description: string, onProgress?: (event: any) => any) {
    let media = await request('post', `/contentmedias`, {
        mediaType,
        [modelName]: modelId,
        user: LoginController.User.id,
        description
    });

    return upload(file, 'contentmedia', media.data.id, 'file', onProgress);
}

export function approveMedia(mediaId: string, approved: boolean) {
    return request('put', `/contentmedias/${ mediaId }`, {
        approved
    });
}

export function deleteMedia(mediaId: string) {
    return request('delete', `/contentmedias/${ mediaId }`);
}

/**
 * Create a new stand in given expo
 * @param expoId Expo id
 * @param stand Stand data
 */
export function addStand(expoId: string, stand: Partial<Stand>): Promise<Stand> {
    invariant(client, `Apollo client not created yet`);
    stand.expo = expoId as any;
    (stand as any).users = LoginController.User.id;
    return client.mutate({
        mutation: gql`mutation AddStand ($stand: StandInput) {
            createStand(input: { data: $stand }) {
                stand { id name }
            }
        }`,
        variables: { stand }
    })
    .then(r => r.data.createStand.stand);
}

/**
 * Edit an stand
 * @param standId Stand id
 * @param stand Stand data
 */
export function editStand(standId: string, stand: Partial<Stand>) {
    return request('put', `/stands/${ standId }`, stand).then(result => result.data as Stand);
}

let config: Promise<Partial<typeof DEFAULT_CONFIG>> = null;
/**
 * Retreive configuration from api
 */
export function getConfig(): Promise<Partial<typeof DEFAULT_CONFIG>> {
    if (config) return config;
    return config = request('get', '/configs?scope=' + APP_NAME).then(result => {
        return { ...(Array.isArray(result.data) && result.data.length > 0 ? result.data[0].values : {}) , ...DEFAULT_CONFIG };
    });
}

export async function getAcl(userId: string) {
    invariant(client, `Apollo client not created yet`);
    const r = await client.query<{
        acls: {
            name: string;
        }[];
    }>({
        query: gql`query Acl ($userId: ID!) { acls(where: { users: $userId, granted: 1 }) { name } }`,
        variables: { userId }
    });
    return r.data.acls.map(o => o.name);
}

export function getStoresGrid(filter?: any, start: number = 0, limit: number = 100) {
    invariant(client, `Apollo client not created yet`);
    filter = { ...filter, users_in: [LoginController.User.id] };
    return client.query<{
        stores: {
            values: Store[],
            aggregate: { totalCount: number }
        }
    }>({
        query: gql`
        query Stores($start: Int, $limit: Int) {
            stores: storesConnection(start: $start, limit: $limit, where: { ${ parepareWhere( filter ) } }) {
                values {
                    id name
                    logo { url }
                    description
                    Type
                    website
                    address
                    province
                    country
                    city
                    phones
                    email
                }
                aggregate { totalCount: count }
            }
        }`,
        variables: { start, limit }
    })
    .then(r => r.data.stores);
}

export async function editStore(store: Store, logo?: File, onProgress?: (event: any) => any) {
    let result = await request('put', `/stores/${ store.id }`, store);
    if ( logo )
        return upload(logo, 'store', result.data.id, 'logo', onProgress);
    return result;
}

export async function newStore(store: Store, logo?: File, onProgress?: (event: any) => any) {
    (store as any).users = [LoginController.User?.id];
    let result = await request('post', `/stores`, store);
    if ( logo )
        return upload(logo, 'store', result.data.id, 'logo', onProgress);
    return result;
}

export const StoreApi = {
    grid: getStoresGrid,
    edit: editStore,
    add: newStore,
    get: (id: string) => {
        return request('get', '/stores/' + id).then(result => result.data as Store);
    }
}

export const CompanyApi = {
    list: companies,
    grid: fetchCompaniesGrid,
    edit: editCompany,
    add: newCompany
}

export const ProductPriceApi = {
    list(storeId: string) {
        return request('get', '/product-prices?store=' + storeId).then(res => res.data as ProductPrice[]);
    },

    add(price: ProductPrice) {
        return request('post', '/product-prices', price);
    }
}

export const ProductApi = {

    /**
     * Products list
     */
    list(filter?: any, start: number = 0, limit: number = 100) {
        invariant(client, `Apollo client not created yet`);
        filter = { ...filter, company_in: LoginController.User.companies.map(c => c.id) }
        return client.query<{ products: {
            values: Product[],
            aggregate: { totalCount: number }
        }}>({
            query: gql`
            query Products($start: Int, $limit: Int) {
                products: productsConnection(start: $start, limit: $limit, where: { ${ parepareWhere( filter ) } }) {
                    values {
                        id
                        name
                        description
                        poster { url}
                        category
                        active
                        # prices: Prices {
                        #     store { id }
                        #     price: Price
                        #     buyType: BuyType
                        #     link: Link
                        # }
                        company { id name logo {url} }
                        images { url }
                    }
                    aggregate { totalCount: count }
                }
            }`,
            variables: { start, limit }
        })
        .then(r => r.data.products);
    },

    get(id: string) {
        return request('get', '/products/' + id);
    },

    edit(product: Partial<Product>) {
        return request('put', `/products/${ product.id }`, product);
    },

    new(product: Partial<Product>) {
        return request('post', `/products`, product);
    }
}

export const CategoryApi = {
    list() {
        invariant(client, `Apollo client not created yet`);
        return client.query<{ categories: { id: string, name: string}[] }>({
            query: gql`query Categories { categories { id name } }`,
            variables: {}
        }).then(result => result.data.categories);
    }
}

export function upload(file: File, ref?: string, refId?: string, field?: string, onProgress?: (event: any) => any) {

    let data = new FormData();
    data.append('ref', ref);
    data.append('refId', refId);
    data.append('files', file);
    data.append('field', field);

    return axios.post(
        `${ apiHost }/upload`,
        data,
        {
            headers: {
                Authorization: `Bearer ${ LoginController.StoredToken }`,
                'Content-Type': 'multipart/form-data'
            },
            onUploadProgress: onProgress
        },
    );
}
