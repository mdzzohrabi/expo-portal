import * as React from 'react';
import { render } from 'react-dom';
import { AppRouter } from './components/router';

import "./style.scss";  // Styles
import { createClient, getConfig } from './api';
import { LoginController } from './components/controller/login';
import { VERSION } from './components/constants';

console.log(`Portal Version: ${ VERSION }`);
console.log(`Application bootstrap loaded`);
console.log(`Environment: ${ process.env.NODE_ENV }`)

window.addEventListener('DOMContentLoaded', function appLoader() {

    // Authentication
    createClient(LoginController.StoredToken);

    // Load configuration
    getConfig().then(config => console.log(`Configuration loaded`));

    render( <AppRouter/>, document.querySelector('#app') );

});