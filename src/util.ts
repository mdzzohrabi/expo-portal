import { message } from "antd";
import { ColumnProps } from "antd/lib/table";
import { ChangeEvent, Component, useCallback, useEffect, useState } from "react";
import { trans } from "./strings";

export function bindTo(component: Component, key: string) {
	return function boundedState(e: ChangeEvent<HTMLInputElement>) {
		component.setState({ [key]: e.target.value });
	}
}

export interface IResizeImageOptions {
	maxSize: number;
	file: File;
}

export const resizeImage = (settings: IResizeImageOptions): Promise<Blob> => {
		const file = settings.file;
		const maxSize = settings.maxSize;
		const reader = new FileReader();
		const image = new Image();
		const canvas = document.createElement('canvas');
		const dataURItoBlob = (dataURI: string) => {
			const bytes = dataURI.split(',')[0].indexOf('base64') >= 0 ?
				atob(dataURI.split(',')[1]) :
				unescape(dataURI.split(',')[1]);
			const mime = dataURI.split(',')[0].split(':')[1].split(';')[0];
			const max = bytes.length;
			const ia = new Uint8Array(max);
			for (var i = 0; i < max; i++) ia[i] = bytes.charCodeAt(i);
			return new Blob([ia], {type:mime});
		};
		const resize = () => {
			let width = image.width;
			let height = image.height;
	
			if (width > height) {
				if (width > maxSize) {
					height *= maxSize / width;
					width = maxSize;
				}
			} else {
				if (height > maxSize) {
					width *= maxSize / height;
					height = maxSize;
				}
			}
	
			canvas.width = width;
			canvas.height = height;
			canvas.getContext('2d').drawImage(image, 0, 0, width, height);
			let dataUrl = canvas.toDataURL('image/jpeg');
			return dataURItoBlob(dataUrl);
		};
	
		return new Promise((ok, no) => {
				if (!file.type.match(/image.*/)) {
					no(new Error("Not an image"));
					return;
				}
	
				reader.onload = (readerEvent: any) => {
					image.onload = () => ok(resize());
					image.src = readerEvent.target.result;
				};
				reader.readAsDataURL(file);
		})    
};	

export function isVideo(url: string) {
	return /\.(mp4|webm)/.test(url);
}

export const Sorter = {

	/**
	 * Sort by date field
	 * @param field Date field name
	 */
	Date(field: string) {
		return function dateSorter(a: any, b: any) {
			return new Date(a[field]) > new Date(b[field]) ? 1 : -1;
		}
	},

	/**
	 * Sort by number field
	 * @param field Number field name
	 */
	Number(field: string) {
		return function dateSorter(a: any, b: any) {
			return Number(a[field]) > Number(b[field]) ? 1 : -1;
		}
	},

	/**
	 * Sort by string field
	 * @param field field name
	 */
	String(field: string) {
		return function dateSorter(a: any, b: any) {
			return String(a[field]) > String(b[field]) ? 1 : -1;
		}
	}

}

export function handleError(err: Error | any) {
	message.error(trans(`An exception occured`));
}

export function tableify(string: string) {
	return string.replace(/\s+(\w)/g, m => m[1].toUpperCase());
}

export function createGridColumns(names: string[], configs: { [name: string]: ColumnProps<any> } = {}) {
	let columns: ColumnProps<any>[] = [];



	return columns;
}


export function useInputChange<T>(defaultValue: T = {} as any): [T, React.Dispatch<React.SetStateAction<T>>, (event: ChangeEvent<HTMLInputElement | HTMLTextAreaElement | HTMLSelectElement>) => void] {
	let [data, setData] = useState<T>(defaultValue);
	
	const handleInputChange = useCallback((e: ChangeEvent<HTMLInputElement | HTMLTextAreaElement | HTMLSelectElement>) => {
		setData({
			...data,
			[e.currentTarget.name]: e.currentTarget.value
		})
	}, [data]);
	
	return [data, setData, handleInputChange];
}

export function useAsyncMemo<T>(_default: T, factory: () => Promise<T>, deps: any[]): [T, boolean, React.Dispatch<React.SetStateAction<T>>] {
	let [value, setValue] = useState<T>(_default);
	let [loading, setLoading] = useState(false);
	useEffect(() => {
		factory().then(result => {
			setValue(result);
			setLoading(false);
		})
	}, deps);
	return [value, loading, setValue];
}

export function intersectKey<T, K extends keyof T>(target: T, keys: K[]): { [k in K]: T[k] } {
	let result: any = {};
	keys.forEach(k => result[k] = target[k]);
	return result;
}