export interface Model {
    id?: string
    createdAt?: string
    updatedAt?: string
}
export interface UploadFile extends Model {
    url?: string
    name?: string
    hash?: string
    ext?: string
    mime?: string
    size?: number
    provider?: string
}

export interface ContentMedia extends Model {
    file?: UploadFile
    expo?: Expo
    company?: Company
    stand?: Stand
    user?: User
    product?: any
    visits?: number
    likes?: number
    description?: string
    approved?: boolean
    // Update approved
    approvedLoading?: boolean
    approvedOn?: string
    mediaType?: string
    thumbnail?: UploadFile
}

export interface Expo extends Model {
    title?: string
    description?: string
    country?: string
    province?: string
    city?: string
    poster?: UploadFile
    startTime?: string
    endTime?: string
    category?: string
    address?: string
    medias?: ContentMedia[]
    stands?: Stand[]
    extra?: any
    images?: UploadFile[]
}

export interface Stand extends Model {
    name?: string
    standNo?: string

    cover?: UploadFile
    address?: string
    company?: Company | string
    expo?: Expo
    description?: string
    introVideo?: UploadFile
    images?: UploadFile
    medias?: ContentMedia[]
}

export interface Company extends Model {
    name?: string
    logo?: UploadFile
    type?: string
    country?: string
    province?: string
    city?: string
    email?: string
    description?: string
    address?: string
    phones?: string
    cover?: UploadFile
    website?: string
    longDescription?: string
    showInCompanies?: boolean
    medias?: ContentMedia[]
}

export interface Store extends Model {
    name?: string
    description?: string
    cover?: UploadFile
    logo?: UploadFile
    type?: string
    country?: string
    phones?: string
    email?: string
    province?: string
    city?: string
    active?: boolean
}

export interface Price extends Model {
    buyType?: string
    link?: string
    price?: number
    store?: Store;
}

export interface Product extends Model {
    name?: string
    description?: string
    poster?: UploadFile
    category?: string
    images?: UploadFile[]
    Prices?: Price[]
    medias?: ContentMedia[]
    active?: boolean
    company?: Company
    users?: User[]
}

export interface Store extends Model {
    name?: string
    logo?: UploadFile
    cover?: UploadFile
    type?: string
    country?: string
    province?: string
    city?: string
    description?: string
    address?: string
    phones?: string
    website?: string
    products?: Product[]
}

export interface User extends Model {
    username?: string
    email?: string
    confirmed?: boolean
    companies?: Company[]
    acl?: { name: string, title: string }[]
    role: {
        name: string
        description: string
        type: string
    }
}

export interface ProductPrice {
    id: string
    store: Store
    product: Product
    price: number
    description: string
    buyType: string
    approved: boolean
    link: string
}